package eip;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.json.simple.parser.JSONParser;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.log4j.BasicConfigurator;
import java.util.Scanner;

public class ProducerConsumer {


	public static void main(String[] args) throws Exception { // Configure le logger par défaut


		BasicConfigurator.configure();

		// Contexte Camel par défaut
		CamelContext context = new DefaultCamelContext();

		// Crée une route contenant le consommateur
		RouteBuilder routeBuilder = new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				// On définit un consommateur 'consumer-1'
				// qui va écrire le message
				from("direct:consumer-1").to("log:affiche-1-log");

				// Question 2
				from("direct:consumer-2").to("file:messages");

				from("direct:consumer-all")
				.choice()
				.when(header("destinataire").contains("écrire"))
				.to("direct:consumer-2")
				.otherwise()
				.to("direct:consumer-1");

				// Question 3a
				from("direct:Citymanager1")
				.setHeader(Exchange.HTTP_METHOD, constant("GET"))
				.setHeader(Exchange.HTTP_URI, simple("http://127.0.0.1:8080/animals/byName/${body}"))
				.to("http://127.0.0.1:8080/animals/byName")
				.to("direct:consumer-1")
				.end();
				
				
				// Question 3b
				from("direct:Citymanager2")
				.setHeader(Exchange.HTTP_METHOD, constant("GET"))
				.setHeader(Exchange.HTTP_URI, simple("http://127.0.0.1:8080/animals/byName/${body}"))
				.to("http://127.0.0.1:8084/animals/byName")
                .to("direct:consumer-1")
                .end();

				from("direct:consumer-geoname")
				.setHeader(Exchange.HTTP_METHOD, constant("GET"))
				.setHeader(Exchange.HTTP_URI, simple("http://api.geonames.org/search?q=${body}&maxRows=1&username=titi"))
				.to("http://api.geoname.org")
				.split(xpath("//lat/text() | //lng/text()"))
				.aggregate(new AggregationStrategy() {

					public Exchange aggregate(Exchange arg0, Exchange arg1) {

						System.out.println(""+"\n" + arg1.getIn().getBody());

						return arg1;
					}
				}).body().completionSize(5)
				.to("direct:consumer-1");


			}
		};

		// On ajoute la route au contexte
		routeBuilder.addRoutesToCamelContext(context);

		// On démarre le contexte pour activer les routes
		context.start();

		// On crée un producteur
		ProducerTemplate pt = context.createProducerTemplate();

		Scanner scan = new Scanner(System.in);
		String entry = scan.nextLine();

		// Question 2
		/*
		do {
			if (entry.startsWith("w")) {
				pt.sendBodyAndHeader("direct:consumer-all",entry , "destinataire", "écrire");
			} else {
				pt.sendBody("direct:consumer-all", entry);
			}
			entry = scan.nextLine();
		} while ( ! entry.equals("exit") );
		 */


		// Question 3a
		//pt.sendBody("direct:Citymanager1", entry);
		
		// Question 3b
		
		// Test geoname
		pt.sendBody("direct:consumer-geoname", entry);
		
		//pt.sendBody("direct:Citymanager2", entry);

		scan.close();
	}
}

